package raflee.study.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import raflee.study.security.domain.AppUser;

public interface UserRepository extends JpaRepository<AppUser,Long>{
	
	AppUser findByUsername(String username);
}
