package raflee.study.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import raflee.study.security.domain.Role;

public interface RoleRepository extends JpaRepository<Role,Long>{
	
	Role findByName(String name);
}
