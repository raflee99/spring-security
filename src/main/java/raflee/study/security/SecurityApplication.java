package raflee.study.security;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import raflee.study.security.domain.AppUser;
import raflee.study.security.domain.Role;
import raflee.study.security.service.UserService;

@SpringBootApplication
public class SecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication.class, args);
	}
	
	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(null,"USER"));
			userService.saveRole(new Role(null,"MANAGER"));
			userService.saveRole(new Role(null,"ADMIN"));
			userService.saveRole(new Role(null,"SUPERADMIN"));
			
			userService.saveUser(new AppUser(null,"Raflee","raf","1234",new ArrayList<>()));
			userService.saveUser(new AppUser(null,"Shafeeq","shaf","1234",new ArrayList<>()));
			
			userService.addRoleToUser("raf", "USER");
			userService.addRoleToUser("raf", "SUPERADMIN");
			userService.addRoleToUser("raf", "ADMIN");
			userService.addRoleToUser("shaf", "	MANAGER");

		};
	}

}
