package raflee.study.security.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

import lombok.RequiredArgsConstructor;


@Configuration @EnableWebSecurity @RequiredArgsConstructor
public class SecurityConfig{	
	
	public SecurityFilterChain configure(HttpSecurity http) throws Exception{
			
		return http
				.csrf(csrf -> csrf.disable())
				.authorizeRequests(auth -> {
					auth.antMatchers("/*").permitAll();
					auth.antMatchers("/admin").hasRole("ADMIN");
				})
				.httpBasic(Customizer.withDefaults())
				.build();
		
		
	}
	
}
