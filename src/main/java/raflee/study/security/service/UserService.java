package raflee.study.security.service;

import java.util.List;

import raflee.study.security.domain.Role;
import raflee.study.security.domain.AppUser;

public interface UserService {
	AppUser saveUser(AppUser user);
	Role saveRole(Role role);
	void addRoleToUser(String username, String roleName);
	AppUser getUser(String username);
	List<AppUser> getUsers();
	
}
