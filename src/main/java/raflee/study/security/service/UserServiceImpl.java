package raflee.study.security.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import raflee.study.security.domain.Role;
import raflee.study.security.domain.AppUser;
import raflee.study.security.repository.RoleRepository;
import raflee.study.security.repository.UserRepository;

@Service @RequiredArgsConstructor @Transactional @Slf4j
public class UserServiceImpl implements UserService{
	
	private final UserRepository userRepo;
	private final RoleRepository roleRepo;

	@Override
	public AppUser saveUser(AppUser user) {
		// TODO Auto-generated method stub
		log.info("Saving new user {} to database",user.getName());
		return userRepo.save(user);
	}

	@Override
	public Role saveRole(Role role) {
		// TODO Auto-generated method stub
		log.info("Saving new role {} to database",role.getName());
		return roleRepo.save(role);
	}

	@Override
	public void addRoleToUser(String username, String roleName) {
		// TODO Auto-generated method stub
		log.info("Adding new role {} to user {}",roleName,username);
		AppUser user = userRepo.findByUsername(username);
		Role role = roleRepo.findByName(roleName);		
		user.getRoles().add(role);
		
	}

	@Override
	public AppUser getUser(String username) {
		// TODO Auto-generated method stub
		log.info("Fetching user {}",username);
		return userRepo.findByUsername(username);
	}

	@Override
	public List<AppUser> getUsers() {
		// TODO Auto-generated method stub
		log.info("Fetching all users");
		return userRepo.findAll();
	}

}
